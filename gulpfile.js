'use strict';

var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var del = require('del');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var sourcemaps = require('gulp-sourcemaps');
var merge = require('merge-stream');
var sequence = require('run-sequence');
var imagemin = require('gulp-imagemin');
var rename = require('gulp-rename');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var csscomb = require('gulp-csscomb');
var cssnano = require('gulp-cssnano');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var swig = require('gulp-swig');
var prettify = require('gulp-prettify');
var watch = require('gulp-watch');
var browsersync = require('browser-sync');
var argv = require('yargs').argv;
var empty = gutil.noop;
var debounceTimeout = null;

var settings = {
   src: './src',
   nodeDir: './node_modules',
   dest: './build',
   browsersync: {
      server: {
         baseDir: './build'
      },
      open: false,
      notify: true
   },
   swig: {
      defaults: {
         cache: false
      }
   }
};

var sources = {
   js: [{
      src: [
         path.join(settings.nodeDir, 'jquery/dist/jquery.js'),
         path.join(settings.src, 'js/script.js'),
         //path.join(settings.src, 'js/some-other-script.js')
      ],
      dest: path.join(settings.dest, 'js/script.js'),
      clean: [
         path.join(settings.dest, 'js')
      ],
      watch: [
         path.join(settings.src, 'js/**/*')
      ]
   }],
   less: [{
      src: [
         path.join(settings.src, 'less/style.less')
      ],
      dest: path.join(settings.dest, 'css/style.css'),
      clean: [
         path.join(settings.dest, 'css')
      ],
      watch: [
         path.join(settings.src, 'less/**/*')
      ]
   }],
   templates: [
      {
         src: path.join(settings.src, 'content/**/*'),
         dest: settings.dest,
         clean: [
            path.join(settings.dest, '*.html')
         ],
         watch: [
            path.join(settings.src, 'content/**/*'),
            path.join(settings.src, 'templates/**/*')
         ]
      }
   ],
   img: [{
      src: [
         path.join(settings.src, 'img/**/*')
      ],
      dest: path.join(settings.dest, 'img'),
      clean: [
         path.join(settings.dest, 'img')
      ],
      watch: [
         path.join(settings.src, 'img/**/*')
      ]
   }],
};

var processImages = empty;

if (argv.deploy) {
   processImages = imagemin;
}

var browsersyncRoot = './build/';
gulp.task('browser-sync', function() {
   browsersync.init(settings.browsersync);
   gulp.watch(path.join(settings.dest, '**/*')).on('change', browsersync.reload);
});

gulp.task('less', function (cb) {
   var files = sources['less'];
   if(files.length > 0) {
      var subtasks = files.map(function (file) {
         return gulp
            .src(file.src)
            .pipe(plumber(settings.plumber))
            .pipe(less(settings.less))
            .pipe(autoprefixer(settings.autoprefixer))
            .pipe(csscomb())
            .pipe(concat(path.basename(file.dest)))
            .pipe(cssnano(settings.cssnano))
            .pipe(gulp.dest(path.dirname(file.dest)));
      });
      return merge.apply(null, subtasks);
   }
   else {
      cb();
   }
});

gulp.task('js', function(){
   var files = sources['js'];
   var targets = Object.keys(files);
   var tasks = targets.map(function (target) {
      var data = files[target];
      return gulp
         .src(data.src)
         .pipe(plumber())
         .pipe(concat(path.basename(data.dest)))
         .pipe(sourcemaps.init({loadMaps: true}))
         .pipe(uglify())
         .pipe(sourcemaps.write('./'))
         .pipe(gulp.dest(path.dirname(data.dest)));
   });
   return merge
      .apply(null, tasks);
});

gulp.task('img', function () {
   var files = sources['img'];
   var targets = Object.keys(files);
   var tasks = targets.map(function (target) {
      var images = files[target];
      return gulp
         .src(images.src)
         .pipe(plumber(settings.plumber))
         .pipe(processImages(settings.imagemin))
         .pipe(gulp.dest(images.dest));
   });
   return merge
      .apply(null, tasks);
});

gulp.task('templates', function (cb) {
   var files = sources['templates'];
   if(files && files.length > 0) {
      var subtasks = files.map(function (file) {
         return gulp
            .src(file.src)
            .pipe(plumber(settings.plumber))
            .pipe(swig(settings.swig))
            .pipe(prettify(settings.prettify))
            .pipe(gulp.dest(file.dest));
      });
      return merge.apply(null, subtasks);
   }
   else {
      cb();
   }
});

gulp.task('clean', function(cb) {
   del(settings.dest);
   cb();
});

gulp.task('watch', function() {
   browsersync.init({
      server: {
         baseDir: browsersyncRoot
      },
      open: argv.browsersyncOpen || false,
      notify: argv.browsersyncNotify || false
   });

   gulp.watch(path.join(settings.dest, '**/*'), function() {
      if (debounceTimeout) {
         clearTimeout(debounceTimeout);
      }

      debounceTimeout = setTimeout(browsersync.reload, 500);
   });

   for (var task in sources) {
      var w = sources[task];
      var files = [];
      for (var i in w) {
         var toWatch = w[i].watch;
         files = files.concat(toWatch);
      }
      gulp.watch(files, [task]);
   }
});

gulp.task('dev', function(cb) {
   sequence('default', 'watch', cb);
});

gulp.task('default', function(cb) {
   sequence('clean', ['js', 'img', 'templates', 'less'], cb);
});